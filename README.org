* Note for develelopers

In a pure environment:
#+begin_src bash
guix shell --pure emacs-bedrock-as-default bash --with-source=emacs-bedrock-base=/home/eagullo/project/gitlab.inria.fr/compose/include/emacs-bedrock/emacs-bedrock-base --with-source=emacs-bedrock-write=/home/eagullo/project/gitlab.inria.fr/compose/include/emacs-bedrock/emacs-bedrock-write --with-source=emacs-bedrock-org=/home/eagullo/project/gitlab.inria.fr/compose/include/emacs-bedrock/emacs-bedrock-org --with-source=emacs-bedrock-dev=/home/eagullo/project/gitlab.inria.fr/compose/include/emacs-bedrock/emacs-bedrock-dev --with-source=emacs-bedrock-dev-minimal=/home/eagullo/project/gitlab.inria.fr/compose/include/emacs-bedrock/emacs-bedrock-dev-minimal  -- bash -c 'GUIX_PROFILE="$HOME/.config/guix/current" ; . "$GUIX_PROFILE/etc/profile" ; emacs --init-dir=~/.config/emacs' &  
#+end_src

In a container:
#+begin_src bash
  guix shell --container --preserve=TERM emacs-bedrock-full --with-source=emacs-bedrock-base=/home/eagullo/project/gitlab.inria.fr/compose/include/emacs-bedrock/emacs-bedrock-base --with-source=emacs-bedrock-write=/home/eagullo/project/gitlab.inria.fr/compose/include/emacs-bedrock/emacs-bedrock-write --with-source=emacs-bedrock-org=/home/eagullo/project/gitlab.inria.fr/compose/include/emacs-bedrock/emacs-bedrock-org --with-source=emacs-bedrock-dev=/home/eagullo/project/gitlab.inria.fr/compose/include/emacs-bedrock/emacs-bedrock-dev --with-source=emacs-bedrock-dev-minimal=/home/eagullo/project/gitlab.inria.fr/compose/include/emacs-bedrock/emacs-bedrock-dev-minimal -- emacs --init-directory=$PWD --debug-init
#+end_src
